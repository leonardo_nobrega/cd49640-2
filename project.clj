(defproject cd49640 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[cc.qbits/alia-all "3.1.11" :exclusions [cc.qbits/tardis]]
                 [cc.qbits/hayt "3.1.0"]
                 [cheshire "5.7.1"]
                 [org.clojure/clojure "1.8.0"]
                 ]
  :aot [cd49640.core]
  :profiles {:repl {:main cd49640.core
                    :target-path "target"}})

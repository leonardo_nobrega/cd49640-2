(ns cd49640.core
  (:require [cheshire.core :as json]
            [qbits.alia :as alia]
            [qbits.hayt :as hayt]))

(defn make-session
  [cassandra-address]
  (->> cassandra-address
       (hash-map :contact-points)
       alia/cluster
       alia/connect))

(defn make-insert
  [id]
  (let [combined-metric-name "AverageOfFive"
        cfg {"_type" "CombinedMeasurement"
             "_base" "SIMMeasurement"
             "_erl" (str "CombinedMeasurement:" id combined-metric-name)
             "name" combined-metric-name
             "associatedSimInstance" id
             "inputMetrics" [{"id" "A" "collectionPeriod" "60"}
                             {"id" "B" "collectionPeriod" "60"}
                             {"id" "C" "collectionPeriod" "60"}
                             {"id" "D" "collectionPeriod" "60"}
                             {"id" "E" "collectionPeriod" "60"}]
             "_id" (str id combined-metric-name)
             "formula" (str "function (A, B, C, D, E) { "
                            "return (A + B + C + D + E) / 5; "
                            "}")}]
  (hayt/insert (keyword "dev17cenx.entity")
               (hayt/values {:id (str id combined-metric-name)
                             :type "CombinedMeasurement"
                             :basetype "SIMMeasurement"
                             :value (json/encode cfg)}))))

(defn- batch-of-inserts
  [batch-size ids]
  (let [make-batch (fn [inserts]
                     (hayt/batch (apply hayt/queries inserts)
                                 (hayt/logged false)))]
    (->> ids
         (map make-insert)
         (partition-all batch-size)
         (map make-batch))))

(defn generate-configs
  [num-rows]
  (let [batch-size 50]
    (with-open [session (make-session "cassandra01-amun.cenx.localnet")]
      (->> num-rows
           range
           (map (partial str "vm"))
           (batch-of-inserts batch-size)
           (map (fn [batch-count batch]
                  (let [num-rows (* batch-count batch-size)]
                    (when (zero? (rem num-rows 10000))
                      (println "done" num-rows "rows")))
                  (alia/execute session batch))
                (range))
           dorun))))

;;; below is code to be used on the levski repl

(defn make-sample
  [metric time id]
  {:id (str id)
   :metric-name (str metric)
   :timestamp (long time)
   :value (double (rand-int 100))})

(defn make-input-samples
  [num-ids]
  (let [ids (map #(str "vm" %) (range num-ids))
        metrics ["A" "B" "C" "D" "E"]
        time (System/currentTimeMillis)]
    (for [id ids m metrics] (make-sample m time id))))

#_(let [ch (-> prod.init/system :combined-metrics :engine-state deref :input-ch)
      num-ids (* 400 1000)
      samples (make-input-samples num-ids)]
  (cenx.levski.combined/push-samples samples ch))
